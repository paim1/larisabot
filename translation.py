import locale
locale.setlocale(locale.LC_ALL, '')

api_key = ("trnsl.1.1.20180213T145255Z.c010874146eb2559"
           ".046db3b8849ba0a30a92913f24102de0750a5afd")

from yandex import Translater


def normalize_lang(lang):
    if lang == 'Russian' or lang == 'RSN':
        lang = 'ru'
    elif lang == 'Tatar' or lang == 'TTR':
        lang = 'tt'
    elif lang == 'Bashkir' or lang == 'PXKR':
        lang = 'ba'
    elif lang == 'Polish' or lang == 'PLX':
        lang = 'pl'
    else:
        raise ValueError('Could not normalize language. Unknown language: '+str(lang))
    return lang
        
def translate(text, to_lang='ru'):
    tr = Translater(key=api_key,
                    from_lang='en',
                    to_lang=to_lang,
                    text=text)

    result = tr.translate()
    print("response json" + str(result))
    return result