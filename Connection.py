# -*- coding: utf-8 -*-
import connection_secret
import pymysql


class User:

    def __init__(self, chat_id):
        self.conn = pymysql.connect(host=connection_secret.host,
                                    user=connection_secret.user,
                                    password=connection_secret.password,
                                    db=connection_secret.db)
        self.curs = self.conn.cursor()
        self.chat_id = chat_id
        if not self.find_user():
            line = ("INSERT INTO `telegram`(`chat_id`, `state`, "
                    "`temp`, `side`) "
                    "VALUES (" + str(chat_id) + ",\"MAIN_MENU\",\"\",1)")
            self.query(line)

    def find_user(self):
        line = "SELECT * FROM telegram WHERE chat_id = " + str(
            self.chat_id) + " ;"
        self.query(line)
        return self.curs.fetchone()

    def connect(self):
        self.conn = pymysql.connect(connection_secret.host,
                                    connection_secret.user,
                                    connection_secret.password,
                                    connection_secret.db)

    def query(self, sql):
        try:
            self.curs.execute(sql)
        except Exception:
            self.connect()
            self.curs = self.conn.cursor()
            self.curs.execute(sql)
        finally:
            self.conn.commit()
            self.curs.close()

    def get_state(self):
        return self.find_user()[1]

    def get_temp(self):
        return self.find_user()[2]

    def get_side(self):
        return self.find_user()[3]

    def send_temp(self, temp):
        line = "UPDATE telegram SET temp = \"" + str(
            temp) + "\" WHERE chat_id = " + str(self.chat_id) + ";"
        self.query(line)
        self.conn.commit()
        self.curs.close()

    def update_side(self, new_side):
        line = "UPDATE telegram SET side = \"" + str(
            new_side) + "\" WHERE chat_id = " + str(self.chat_id) + ";"
        self.query(line)
        self.conn.commit()
        self.curs.close()

    def update_state(self, new_state):
        line = "UPDATE telegram SET state = \"" + str(
            new_state) + "\" WHERE chat_id = " + str(self.chat_id) + ";"
        self.query(line)
        self.conn.commit()
        self.curs.close()

    def set_lang_to_translate(self, lang_code):
        self.send_temp(lang_code)
