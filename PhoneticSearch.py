# -*- coding: utf-8 -*-
from metaphone import doublemetaphone

# Game Keywords
game_keywords = {
    'matches': [
        doublemetaphone("matches"),
        doublemetaphone("matchsticks"),
        doublemetaphone("twenty one"),
        doublemetaphone("twenty one matchsticks"),
        doublemetaphone("play matches"),
        doublemetaphone("play matchsticks"),
        doublemetaphone("play twenty one"),
        doublemetaphone("play twenty one matchsticks")
    ],
    'tic-tac-toe': [
        doublemetaphone("tic-tac-toe"),
        doublemetaphone("three x three"),
        doublemetaphone("play tic-tac-toe"),
        doublemetaphone("play three x three")
    ],
    'gomoku': [
        doublemetaphone("gomoku"),
        doublemetaphone("five in row"),
        doublemetaphone("play gomoku"),
        doublemetaphone("play five in row")
    ]
}
other_keys = {
    'math': [
        doublemetaphone("calculate"),
        doublemetaphone("compute"),
        doublemetaphone("evaluate"),
        doublemetaphone("integrate"),
        doublemetaphone("derive")
    ],
    'translate': [
        doublemetaphone("translate")
    ]
}

languages = [
    doublemetaphone("Russian"),
    # doublemetaphone("Russkii"),
    doublemetaphone("Polish"),
    # doublemetaphone("Polski"),
    doublemetaphone("Tatar"),
    # doublemetaphone("Bashkort"),
    # doublemetaphone("Bashkirian"),
    doublemetaphone("Bashkir")
]

# for k, v in keywords.items():
#     for v1 in v:
#         print(v1[0])

# print(doublemetaphone("Matches"))
# print(doublemetaphone("match this"))
# print()
# print(doublemetaphone("matches"))
# print(doublemetaphone("matchsticks"))
# print(doublemetaphone("twenty one"))
# print(doublemetaphone("twenty one matchsticks"))

# print(doublemetaphone("message text"))
