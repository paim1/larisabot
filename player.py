# -*- coding: utf-8 -*-
from collections import Counter

x_player = "x"
o_player = "o"
empty = "."


def get_empty_field():
    return [empty]*9


def get_stats(field):
    xo_count = Counter(field)
    return xo_count


def is_valid_field(field):
    stats = get_stats(field)
    if (stats[x_player] + stats[o_player] + stats[empty]) != 9:
        return False
    if (stats[x_player] - stats[o_player]) != 0 and (
            stats[x_player] - stats[o_player] != 1):
        return False
    return True


def i_won(field):
    wins = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7],
            [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    for win in wins:
        if field[win[0]] == field[win[1]] == field[win[2]] != empty:
            return field[win[2]]
    return False


def play_dont_check(field=get_empty_field()):
    # field itself is valid
    if not is_valid_field(field):
        return "Field is invalid", field
    stats = get_stats(field)
    enemy, me = (x_player, o_player) if (
            stats[x_player] > stats[o_player]) else (o_player, x_player)
    if i_won(field) == enemy:
        return "You won. Congrats!", field
    if empty not in field:
        return "Draw", field
    won, index = next_move(field, me)
    field[index] = me
    if i_won(field) == me:
        return "I won. Try again;)", field
    if empty not in field:
        return "Draw", field
    return field


def next_move(board, player):
    if board[4] == empty:
        return 0, 4
    if i_won(board):
        if player is x_player:
            return -1, -1
        else:
            return 1, -1
    # list for appending the result
    res_list = []
    c = board.count(empty)
    if c is 0:
        return 0, -1
    # list for storing the indexes where '-' appears
    _list = []
    for i in range(len(board)):
        if board[i] == empty:
            _list.append(i)
    next_player = x_player if player == o_player else o_player
    for i in _list:
        board[i] = player
        ret, _ = next_move(board, next_player)
        res_list.append(ret)
        board[i] = empty
    if player is x_player:
        max_el = max(res_list)
        return max_el, _list[res_list.index(max_el)]
    else:
        min_el = min(res_list)
        return min_el, _list[res_list.index(min_el)]


def game():
    board = play_dont_check([x_player, empty, o_player,
                             o_player, o_player, empty,
                             empty, x_player, x_player])
    print(board)
    print(play_dont_check(list('x....o.xo')))
    print(play_dont_check(list('oxx...xoo')))
    print(play_dont_check(list('xx...oo.o')))
    # assert nextMove('X--OO-O--','X') ==-1
    print(play_dont_check(list('.........')))
    print(play_dont_check(list('xxooo.xo.')))
    print(play_dont_check(list('xo..xx..o')))
    print(play_dont_check(list('xx.oo.xo.')))
    print(play_dont_check(list('xx..o.xoo')))

#game()
