# -*- coding: utf-8 -*-
import Constants
import translation
import PhoneticSearch
import Levenstein
import SyntaxTree
from metaphone import doublemetaphone


class Intent:

    def __init__(self):
        self.intent = None
        self.params = {}

    def intent_detected(self):
        return self.intent is not None

    def get_intent(self):
        return self.intent

    def get_param(self, key):
        return self.params[key]

    def detect_intent(self, message_text):
        if self.detect_first(message_text):
            print("first detected!")
            return
        message_text = SyntaxTree.preprocess(message_text)
        double_text = doublemetaphone(message_text)

        min_dist = 10**9
        phrase = ""
        for key, values in PhoneticSearch.game_keywords.items():
            for value in values:
                dist = Levenstein.iterative_levenshtein(double_text[0], value[0])
                if min_dist > dist:
                    min_dist = dist
                    phrase = key
                    # print("new phrase: " + phrase)
        print("user, presumably, wants: " + phrase)
        print("the distance is: " + str(min_dist))

        if phrase == 'matches':
            self.intent = Constants.PLAY_GAME
            self.params['game_name'] = Constants.MATCHES
        elif phrase == 'tic-tac-toe':
            self.intent = Constants.PLAY_GAME
            self.params['game_name'] = Constants.TIC_TAC_TOE
        elif phrase == 'gomoku':
            self.intent = Constants.PLAY_GAME
            self.params['game_name'] = Constants.TIC_TAC_TOE_5_IN_ROW

    def detect_first(self, message):
        first = message.split(' ')
        print(first)
        first = [doublemetaphone(word) for word in first]
        min_dist = (10**9, -1)
        phrase = ""
        for key, values in PhoneticSearch.other_keys.items():
            for value in values:
                dist = [Levenstein.iterative_levenshtein(k[0], value[0]) for k in first]
                if min_dist[0] >= min(dist):
                    min_dist = (min(dist), dist.index(min(dist)))
                    phrase = key
        print("user, presumably, wants: " + phrase)
        print("the distance is: " + str(min_dist[0]))
        if min_dist[0] > 1:
            print("no! too far away from what I heard")
            return False
        if phrase == "math":
            self.intent = Constants.SOLVE_MATH
            self.params['expression'] = ' '.join(message.split(' ')[min_dist[1]:])
            print("calculate: " + ' '.join(message.split(' ')[min_dist[1]:]))
            return True

        # Only translation left
        #   need to identify language
        first = first[min_dist[1]+1:]
        message = ' '.join(message.split(' ')[min_dist[1]+1:])
        min_dist, phrase = (10**9, -1), ""
        for lang in PhoneticSearch.languages:
            print(lang)
            dist = [Levenstein.iterative_levenshtein(k[0], lang[0]) for k in first]
            if min_dist[0] > min(dist):
                min_dist = (min(dist), dist.index(min(dist)))
                phrase = lang
        print("it looks like: " + phrase[0] + " - " + first[min_dist[1]][0])
        self.intent = Constants.TRANSLATE
        to_translate = message.split(' ')
        to_translate.pop(min_dist[1])
        self.params['phrase_to_translate'] = ' '.join(to_translate)
        print("to translate: " + ' '.join(to_translate))
        self.params['to_lang'] = translation.normalize_lang(phrase[0])
        print("___NORMALIZED")
        return True

    # def try_to_detect_intent(self, message_text):
    #     import SyntaxTree
    #     message_text = SyntaxTree.preprocess(message_text)
    #
    #     print("detecting intent in " + message_text)
    #
    #     match_command = re.compile(
    #         '(?:^|\s)(translate|play|calculate|compute|evaluate|estimate|solve)\s+([a-z0-9\-\s]+)',
    #         re.IGNORECASE).search(message_text)
    #     # print(match_command.group(1).lower().__len__())
    #     # print(match_command.group(1).lower())
    #     # print(match_command.group(2))
    #     if match_command:
    #         if match_command.group(1).lower() in {'translate'}:
    #             self.try_parse_translate(match_command.group(2))
    #         elif match_command.group(1).lower() in {'play'}:
    #             print("going to play")
    #             self.try_parse_play(match_command.group(2))
    #         elif match_command.group(1).lower() in {'calculate', 'compute', 'evaluate', 'estimate', 'solve'}:
    #             self.try_parse_solve(match_command.group(2))

    # def try_parse_translate(self, text_after_translate):
    #     match_translate1 = re.compile('(?:^|\s)into\s+(Russian|Polish|Tatar|Bashkir)\s+(.+)', re.IGNORECASE).match(
    #         text_after_translate)
    #     match_translate2 = re.compile('(.+)into\s+(Russian|Polish|Tatar|Bashkir)$', re.IGNORECASE).match(
    #         text_after_translate)
    #     if match_translate1:
    #         intent = Constants.TRANSLATE
    #         phrase_to_translate = match_translate1.group(2)
    #         to_lang = match_translate1.group(1)
    #     elif match_translate2:
    #         intent = Constants.TRANSLATE
    #         phrase_to_translate = match_translate2.group(1)
    #         to_lang = match_translate2.group(2)
    #     if intent == Constants.TRANSLATE:
    #         try:
    #             to_lang = translation.normalize_lang(to_lang)
    #         except ValueError:
    #             to_lang = None
    #         if to_lang:
    #             self.intent = intent
    #             self.params['phrase_to_translate'] = phrase_to_translate
    #             self.params['to_lang'] = phrase_to_translate

    # def try_parse_solve(self, text_after_solve):
    #     self.intent = Constants.SOLVE_MATH
    #     self.params['expression'] = text_after_solve
