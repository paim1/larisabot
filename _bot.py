# -*- coding: utf-8 -*-
import subprocess

import requests
from bs4 import BeautifulSoup
import telebot
import re
import Keyboards
import Matches
from Connection import User
import player
import Math
import Constants
import Gomoku
import Voice

import SyntaxTree
from Intent import Intent
import logging
import time
from random import randint
import translation
import connection_secret
import os


from telebot import apihelper

# innopolis:innopolis@35.187.119.223:1080

# from urllib3.contrib.socks import SOCKSProxyManager
# proxy = SOCKSProxyManager('socks5://213.134.203.146:39880/')
# res = proxy.request('GET', 'http://httpbin.org/ip')
# print(res.data)

try:
    import urllib.parse
except ImportError:
    import urlparse

bot = telebot.TeleBot(connection_secret.TAPI)

API = "http://api.wolframalpha.com/v2/query?input={}&appid={}"


def ask(query, id=0):
    print(id, " Asking:", query)
    try:
        resp = requests.get(
            API.format(urllib.parse.quote_plus(query), connection_secret.WAPI))
        if resp.status_code != 200:
            return None
        dom = BeautifulSoup(resp.text, "lxml")
        result = dom.queryresult.findAll("pod", id="Solution")
        if not result:
            result = dom.queryresult.findAll("pod", id="Result")
        if not result:
            result = dom.queryresult.findAll(
                "pod",
                id="ChemicalNamesFormulas:ChemicalData")
        if not len(result):
            return None

        subpods = result[0].findAll("subpod")
        return list(pod.plaintext.string for pod in subpods)
    except Exception as ex:
        return ex


def start_game(user, game_name):
    if game_name == Constants.TIC_TAC_TOE:
        res = Constants.MSG_CHOOSE_SIDE
        markup = Keyboards.choose_game_side_keyboard()
        user.update_state(
            Constants.GAME + Constants.TIC_TAC_TOE +
            Constants.CHOOSE_SIDE)
    elif game_name == Constants.TIC_TAC_TOE_5_IN_ROW:
        res = Constants.MSG_CHOOSE_SIDE
        markup = Keyboards.choose_game_side_keyboard()
        user.update_state(
            Constants.GAME + Constants.TIC_TAC_TOE_5_IN_ROW +
            Constants.CHOOSE_SIDE)
    elif game_name == Constants.MATCHES:
        res = Constants.MSG_CHOOSE_ORDER
        markup = Keyboards.choose_turn_order_keyboard()
        user.update_state(
            Constants.GAME + Constants.MATCHES + Constants.CHOOSE_SIDE)
    return res, markup


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    user = User(message.chat.id)
    user.update_state(Constants.MAIN_MENU)
    markup = Keyboards.main_menu_keyboard()
    bot.reply_to(message,
                 "Hi! My name is " + Constants.APP_NAME +
                 "! I can do math and play game!",
                 reply_markup=markup)


@bot.message_handler(content_types=['audio', 'voice'])
def handle_docs_audio(message):
    file_info = bot.get_file(message.voice.file_id)
    downloaded_file = bot.download_file(file_info.file_path)
    res = "Error here"
    with open(str(message.chat.id) + '.ogg', 'wb') as new_file:
        new_file.write(downloaded_file)
    try:
        responce = Voice.ask_yandex(str(message.chat.id) + '.ogg', Voice.UUID)
        # Voice.analyse_responce(responce)
        user = User(message.chat.id)
        user.send_temp(Voice.analyse_responce(responce))
        # Voice.write_to_text(responce)
        reply = ""
        # new_file = open(Voice.STORAGE, 'r')
        reply += user.get_temp()
        # svo.fast_parse(open(Voice.STORAGE).readlines()[0])
        # SyntaxTree.fast_parse(Voice.STORAGE)

        intent = Intent()
        print(reply)
        intent.detect_intent(reply)
        print(intent)

        if intent.get_intent() == Constants.TRANSLATE:
            print("___(1/4)GOT INTO STATE")
            intent_translate = intent.get_param('phrase_to_translate')
            print("___(2/4)GOT COMMAND")
            intent_to = intent.get_param('to_lang')
            print("___(3/4)GOT TARGET LANGUAGE")
            res = "".join(translation.translate(intent_translate, intent_to))
            print("___(4/4)GOT RESPONSE")
        elif intent.get_intent() == Constants.PLAY_GAME:
            user = User(message.chat.id)
            res, markup = start_game(user, intent.get_param('game_name'))
            bot.reply_to(message, res, reply_markup=markup)
            return
        elif intent.get_intent() == Constants.SOLVE_MATH:
            try:
                res = Math.solve(intent.get_param('expression'))
            except Exception:
                res = ("Even the highly educated artificial intelligence "
                       "may dont know something, but i can solve simpler "
                       "tasks")
    except Exception:
        res = "Can not recognize you :("
    bot.reply_to(message, res, reply_markup=None)


@bot.message_handler(content_types=['photo'])
def handle_images(message):
    user = User(message.chat.id)
    state = user.get_state()
    res = "Hello. i receive images from \" " + Constants.PICTURES + " \" menu only"
    markup = Keyboards.go_back_keyboard()
    if state == Constants.PIC:
        file_info = bot.get_file(message.photo[-1].file_id)
        downloaded_file = bot.download_file(file_info.file_path)
        with open(str(message.chat.id) + '.jpg', 'wb') as new_file:
            new_file.write(downloaded_file)

        request = ('docker cp ' + str(message.chat.id) + '.jpg' + ' ' +
                    Constants.DOCKER_ID + ':' + Constants.IMAGE_PATH +
                    str(message.chat.id) + '.jpg && '
                   'docker exec -it 86a11187cae1 bash -c "' +
                   '/root/openface/demos/classifier.py infer ' +
                   '/root/openface/data/celebdata/feature/classifier.pkl '
                   + Constants.IMAGE_PATH + str(message.chat.id) + '.jpg"')
        f = os.popen(request)
        ans = f.read()
        name = [s for s in ans.split('\n') if "Predict" in s][0]
        print(name)
        name = name.replace('Predict', '')\
                   .replace('with', '')\
                   .replace('confidence', '')
        conf = float(re.findall("\d+\.\d+", name)[0])
        if conf < 0.5:
            res = "Seems like I don't know who that was!"
        else:
            res = "Oh! I see " + \
                  name.replace(str(conf), '')\
                      .replace(' ', '')\
                      .replace('.', '') + \
                  " on this picture!\n" + \
                  "my confidence is " + str(conf)

    bot.send_message(message.chat.id, res, reply_markup=markup)


@bot.message_handler(func=lambda message: True)
def reply(message):
    res = Constants.MSG_USE_MENU
    user = User(message.chat.id)
    state = user.get_state()
    markup = Keyboards.main_menu_keyboard()
    print(message)

    if state == Constants.MAIN_MENU:
        if message.text == Constants.PLAY_GAME:
            user.update_state(Constants.GAME)
            res = Constants.MSG_CHOOSE_GAME
            markup = Keyboards.choose_game_keyboard()
        elif message.text == Constants.SOLVE_MATH:
            user.update_state(Constants.MATH)
            res = Constants.INVITATION_TO_INPUT_FORMULA
            markup = Keyboards.go_back_keyboard()
        elif message.text == Constants.TRANSLATE:
            user.update_state(Constants.CHOOSE_LANG)
            res = Constants.INVITATION_TO_TRANSLATE
            markup = Keyboards.get_translator_lang_keyboard()
        elif message.text == Constants.PICTURES:
            user.update_state(Constants.PIC)
            res = Constants.INVITATION_TO_SEND_PICTURE
            markup = Keyboards.go_back_keyboard()
        else:
            intent = Intent()
            print(message)
            intent.detect_intent(message.text)
            print(intent.get_intent())

            if intent.get_intent() == Constants.TRANSLATE:
                res = translation.translate(intent.get_param('phrase_to_translate'), intent.get_param('to_lang'))
            elif intent.get_intent() == Constants.PLAY_GAME:
                res, markup = start_game(user, intent.get_param('game_name'))
            elif intent.get_intent() == Constants.SOLVE_MATH:
                try:
                    res = Math.solve(intent.get_param('expression'))
                except Exception:
                    res = ("Even the highly educated artificial intelligence "
                           "may dont know something, but i can solve simpler "
                           "tasks")
    elif (state == Constants.GAME + Constants.TIC_TAC_TOE_5_IN_ROW +
          Constants.CHOOSE_SIDE):
        if message.text == Constants.X or message.text == Constants.O:
            user.update_state(Constants.GAME + Constants.TIC_TAC_TOE_5_IN_ROW)
            user.update_side(emojii_to_text(message.text))
            std_board = ["." for x in range(64)]
            if emojii_to_text(message.text) == "o":
                std_board = larisa_make_move(std_board, False)
            user.send_temp(std_board)
            res = Constants.MSG_FIVE_IN_ROW
            markup = Keyboards.inline_key_8x8(std_board)
            bot.send_message(message.chat.id, res, reply_markup=markup)

            res = Constants.MSG_YOU_CAN_GO_BACK
            markup = Keyboards.go_back_keyboard()

            user.update_state(Constants.GAME + Constants.TIC_TAC_TOE_5_IN_ROW)
        elif message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
    elif (state == Constants.GAME + Constants.TIC_TAC_TOE +
          Constants.CHOOSE_SIDE):
        if message.text == Constants.X or message.text == Constants.O:
            res = Constants.MSG_PLAY
            std_board = ["." for x in range(9)]
            if emojii_to_text(message.text) == "o":
                std_board = larisa_make_move(std_board, True)
            user.send_temp(std_board)
            user.update_state(Constants.GAME + Constants.TIC_TAC_TOE)
            markup = Keyboards.inline_key_3x3(std_board)
            bot.send_message(message.chat.id, res, reply_markup=markup)
            res = Constants.MSG_YOU_CAN_GO_BACK
            markup = Keyboards.go_back_keyboard()
            user.update_side(emojii_to_text(message.text))
        elif message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
    elif state == Constants.GAME + Constants.MATCHES + Constants.CHOOSE_SIDE:
        if message.text == Constants.ONE or message.text == Constants.TWO:
            standard_board = Constants.STD_MATCHES_BOARD
            user.send_temp(standard_board)
            user.update_state(Constants.GAME + Constants.MATCHES)
            side = int(message.text)
            user.update_side(side)

            markup = Keyboards.matches_keyboard()
            res = ("Matches!\nYou have {} matchsticks in the beginning!"
                   "\nEach move you can remove from 1 to 3 mathes\n"
                   "The one who takes the last matchstick "
                   "lost\n\n Lets try!\n\n\n").format(standard_board)
            if side == 2:
                b = standard_board
                standard_board = Matches.player_move(standard_board,
                                                     Matches.move(
                                                         standard_board,
                                                         randint(1, 3)))
                res += "\n Hey its my turn!\n\n"
                res += "I take " + str(b - standard_board) + "\n\n"
                res = res + Matches.print_board(standard_board) + "\n\n"
                user.send_temp(standard_board)
            res += ("|" * standard_board) + "\nOk, " + str(
                standard_board) + " matches. And its your turn"
        elif message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
    elif state == Constants.CHOOSE_LANG:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
        else:
            user.set_lang_to_translate(
                Constants.values[message.text])
            user.update_state(Constants.TRANSLATION)
            res = Constants.MSG_TRANSLATION
            markup = Keyboards.go_back_keyboard()
    elif state == Constants.TRANSLATION:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
        else:
            res = translation.translate(message.text, user.get_temp())
            markup = Keyboards.go_back_keyboard()
    elif state == Constants.PIC:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
        else:
            res = "You can send me image here"
            markup = Keyboards.go_back_keyboard()
    elif state == Constants.MATH:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
        else:
            try:
                res = Math.solve(message.text)
            except Exception:
                res = ("Even the highly educated artificial intelligence "
                       "may dont know something, but i can solve simpler "
                       "tasks")
            markup = Keyboards.go_back_keyboard()
    elif state == Constants.GAME:
        try:
            res, markup = start_game(user, message.text)
        except UnboundLocalError:
            pass
    elif state == Constants.GAME + Constants.MATCHES:
        if message.text != Constants.BACK:
            res, markup = matches_i_first(message)
        else:
            res, markup = go_to_main_menu(message)

    elif state == Constants.GAME + Constants.TIC_TAC_TOE:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
    elif state == Constants.GAME + Constants.TIC_TAC_TOE_5_IN_ROW:
        if message.text == Constants.BACK:
            res, markup = go_to_main_menu(message)
    bot.send_message(message.chat.id, res, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def test_callback(call):
    user = User(call.message.chat.id)
    button_pressed = call.data
    button_pressed_text = button_pressed
    is_simple_game = False
    if button_pressed_text[0] != "T":
        button_pressed = int(button_pressed)
    else:
        is_simple_game = True
        button_pressed = int(button_pressed[1])
    chat_id = call.message.chat.id
    message_id = call.message.message_id

    gameplay_board = eval(user.get_temp())
    side = user.get_side()

    if gameplay_board[button_pressed] == ".":

        gameplay_board = i_make_move(gameplay_board, side, button_pressed)
        refresh_keyboard(chat_id, message_id, is_simple_game, gameplay_board)
        gameplay_board = larisa_make_move(gameplay_board, is_simple_game)
        check_game_end(gameplay_board, is_simple_game, call, chat_id,
                       message_id)
    else:
        pass
    user.send_temp(gameplay_board)


def go_to_main_menu(message):
    user = User(message.chat.id)
    user.update_state(Constants.MAIN_MENU)
    markup = Keyboards.main_menu_keyboard()
    reply = Constants.GO_TO_MAIN_MENU
    return reply, markup


def emojii_to_text(emojii):
    if emojii == Constants.X:
        return "x"
    elif emojii == Constants.O:
        return "o"


def get_opposite_side(side):
    if side == "o":
        return "x"
    elif side == "x":
        return "o"


def matches_i_first(message):
    user = User(message.chat.id)
    somebody_won = False
    board = int(user.get_temp())
    res = "You took " + message.text + " matches\n\n"
    try:
        board = Matches.player_move(board, int(message.text))
        res += Matches.print_board(board)
        bot.send_message(message.chat.id, res)
        if board <= 0:
            res += "\n\nI won!"
            somebody_won = True
            bot.send_message(message.chat.id, res)
            res, markup = go_to_main_menu(message)
        else:
            b = board
            board = Matches.player_move(board,
                                        Matches.move(board, int(message.text)))
            res = "I take " + str(b - board) + "\n\n"
            res = res + Matches.print_board(board)
            if board <= 0:
                res += "\n\nYou won!"
                somebody_won = True
                bot.send_message(message.chat.id, res)
                res, markup = go_to_main_menu(message)
            else:
                res += "\n\nYour turn!)"
    except Exception:
        res += "\nOk, something went wrong"
    if not somebody_won:
        user.send_temp(board)
        markup = Keyboards.matches_keyboard()

    return res, markup


def larisa_make_move(gameplay_board, is_simple_game):
    if is_simple_game:
        gameplay_board = player.play_dont_check(gameplay_board)
    else:
        gameplay_board = Gomoku.make_move(gameplay_board)
    return gameplay_board


def i_make_move(gameplay_board, side, button_pressed):
    if side:
        gameplay_board[button_pressed] = side
    else:
        gameplay_board[button_pressed] = 'x'
    return gameplay_board


def check_game_end(gameplay_board, is_simple_game, call, chat_id, message_id):
    if isinstance(gameplay_board, list):
        refresh_keyboard(chat_id, message_id, is_simple_game, gameplay_board)
    else:
        try:
            refresh_keyboard(chat_id, message_id, is_simple_game,
                             gameplay_board[1])
        except Exception:
            pass
        bot.send_message(call.message.chat.id, str(gameplay_board[0]))
        reply, markup = go_to_main_menu(call.message)
        bot.send_message(call.message.chat.id, reply, reply_markup=markup)


def refresh_keyboard(chat_id, message_id, is_simple_game, gameplay_board):
    if is_simple_game:
        bot.edit_message_reply_markup(chat_id=chat_id,
                                      message_id=message_id,
                                      reply_markup=Keyboards.inline_key_3x3(
                                          board=gameplay_board))
    else:
        bot.edit_message_reply_markup(chat_id=chat_id,
                                      message_id=message_id,
                                      reply_markup=Keyboards.inline_key_8x8(
                                          board=gameplay_board))

#
while True:
    try:
        apihelper.proxy = {'https': 'socks5://swcbbabh:aYEbh6q5gQ@176.31.101.162:3306'} # from @FCK_RKN_bot
        bot.polling(none_stop=True)
    except Exception as e:
        bot.stop_polling()
        logging.error(e)
        time.sleep(15)
