import copy
import re
import time

w, h = 8, 8
in_a_row = 5
max_depth = 5
weights = {0: 10 ** 9, 1: 10 ** 6, 2: 10 ** 6, 3: 600, 4: 100, 5: 180, 6: 70, 7: 5, 8: 1}
patterns = [re.compile('xxxxx'), re.compile('_xxxx_|x_xxx_x'),
            re.compile('_xxxx|xxxx_|x_xxx|xxx_x|xx_xx'),
            re.compile('_xxx__|__xxx_|_xx_x_|_x_xx_|x_x_x_x'),
            re.compile('__xx__|_xx___|___xx_|__x_x_|_x_x__|_x__x_'),
            re.compile('_xxx_|xxx__|__xxx|xx_x_|x_xx_|_x_xx|_xx_x|x__xx|xx__x'),
            re.compile('___xx|xx___|_x_x_|x_x__|__x_x'),
            re.compile('___x___'),
            re.compile('__x__')]


def make_move(external_board):
    # print(external_board)
    start = int(round(time.time() * 1000))
    me = 'o' if external_board.count('x') > external_board.count('o') else 'x'

    internal_board = [[0 if external_board[i + j * h] == '.' else
                       1 if external_board[i + j * h] == me else
                       -1
                       for i in range(h)] for j in range(w)]
    if evaluate(internal_board, patterns_weight=patterns) < -10 ** 8:
        return "I lost, damnit!", external_board

    if len(get_valid_moves(internal_board)) == 0:
        return "I guess it is a draw, my friend", external_board

    score, move = minimax(internal_board, sign=1, depth=0, start=start)
    external_board[move[0] * w + move[1]] = me
    internal_board[move[0]][move[1]] = 1

    if evaluate(internal_board, patterns_weight=patterns) > 10 ** 8:
        return "I won, mwa-ha-hah", external_board
    elif len(get_valid_moves(internal_board)) == 0:
        return "I guess it is a draw, my friend", external_board
    else:
        return external_board


def minimax(board, sign, depth, start):
    if (depth >= max_depth or (start + 2000) <
            int(round(time.time() * 1000)) or
            game_over(board)
    ):
        return evaluate(board, patterns), None
    moves = get_valid_moves(board)
    if len(moves) == 0:
        return 0, None

    best_move = moves[0]
    best_score = sign * -10 ** 9
    for move in moves:
        clone = fake_move(board, move, sign)
        clone_score = minimax(clone, -1 * sign, depth + 1, start)[0]
        if clone_score * sign > best_score * sign:
            best_score = clone_score
            best_move = move
    return best_score, best_move


def get_valid_moves(board):
    moves = []
    if board[4][4] == 0:
        moves = [(4, 4)]
    elif board[3][3] == 0:
        moves = [(3, 3)]
    # traverse_length = 1
    for i in range(h):
        for j in range(w):
            if (board[max(0, i - 1)][j] != 0 or board[i][max(0, j - 1)] != 0 or
                board[min(h - 1, i + 1)][j] != 0 or
                board[i][min(w - 1, j + 1)] != 0 or
                board[max(0, i - 1)][max(0, j - 1)] != 0 or
                board[max(0, i - 1)][min(w - 1, j + 1)] != 0 or
                board[min(h - 1, i + 1)][max(0, j - 1)] != 0 or
                board[min(h - 1, i + 1)][min(w - 1, j + 1)] != 0) and (
                    board[i][j] == 0):
                moves.append((i, j))
    return moves


def fake_move(board, move, me):
    new_board = copy.deepcopy(board)
    new_board[move[0]][move[1]] = me
    return new_board


def evaluate(board, patterns_weight):
    char_mat = ['' for i in range(8)]
    for i in range(h):
        for j in range(w):
            # if it's my point (1) -> x
            char_mat[i] += '_' if board[i][j] == 0 else (
                'x' if board[i][j] == 1 else 'o')
    score = eval_patterns(char_mat, patterns_weight)
    for i in range(h):
        char_mat[i] = char_mat[i].replace('x', 'y')
        char_mat[i] = char_mat[i].replace('o', 'x')
    score -= eval_patterns(char_mat, patterns_weight) * 2
    return score


# Built under an assumption that w == h
def eval_patterns(char_mat, patterns_weight):
    score_sum = 0
    # Check rows
    for i in range(h):
        score_sum += pattern_score(char_mat[i], patterns_weight)

    # Check the columns
    for j in range(w):
        row = ''.join([row[j] for row in char_mat])
        score_sum += pattern_score(row, patterns_weight)

    # Check the diagonals
    for i in range(h - in_a_row + 1):
        # the "normal" diagonals
        diag = ''.join([char_mat[i + d][d] for d in range(min(w, h - i))])
        score_sum += pattern_score(diag, patterns_weight)
        # other half as well
        diag = ''.join(
            [char_mat[d][i + 1 + d] for d in range(min(h, w - (i + 1)))])
        score_sum += pattern_score(diag, patterns_weight)
        # the "opposite" diagonals
        diag = ''.join(
            [char_mat[h - (i + d + 1)][d] for d in range(min(w, h - i))])
        score_sum += pattern_score(diag, patterns_weight)
        # other half as well
        diag = ''.join(
            [char_mat[h - (d + 1)][i + 1 + d] for d in range(
                min(h, w - (i + 1)))])
        score_sum += pattern_score(diag, patterns_weight)

    return score_sum


def pattern_score(row, patterns_weight):
    ans = 0
    for i in range(len(patterns_weight)):
        if patterns_weight[i].search(row):
            ans += weights[i]
    return ans


def game_over(board):
    return abs(evaluate(board, [patterns[0]])) > 10 ** 8
