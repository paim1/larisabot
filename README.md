# LarisaBot #

Practical AI team: M1
* Alexandr Kriminetskiy
* Ruslan Gaifullin
* Dilyara Altynbaeva
* Rafis Ganeev

**LarisaBot** is a telegram bot which has following capabilities:

* It explains who is she
* It can solve simple equations and math tasks (integrate, derive, ...)
* It can play "tic-tac-toe" and XO 5-in-a-row game
* It can play 21 matchsticks game

## Build & Run ##
Larisa runs on python3 and uses mysql connection 
and external libraries (see requirements.txt)

Install all the required libraries:
```sh
pip3 install -r requirements.txt
```

You will need MySQL Server 5.1+. Import **db_create** script into the database.
Configure database connection parameters in **connection_secret.py** using provided
**connection_secret.example** as an example:
```sh
cp connection_secret.example connection_secret.py
```

Configure Telegeram Bot key `TAPI` and Wolfram API ID `WAPPID` in **Constants.py**.

And then run the bot:

```sh
run _bot.py
```

To stop the bot while she is running use Ctrl-C hotkey.