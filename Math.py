# -*- coding: utf-8 -*-
import wolframalpha
import Constants
import connection_secret

wolfram_client = wolframalpha.Client(connection_secret.WAPPID)


def solve(text):
    res = wolfram_client.query(text)
    solution = "Here you go, pal. \n"

    if res.success == 'false':
        oops = 'Sorry to disappoint you, I can not solve it. ' + \
               'Perhaps, you should be more specific :)\n'

        if hasattr(res, 'didyoumeans'):
            oops += "But! Here's what I think you wanted: " + \
                    res.didyoumeans['didyoumean'][0]['#text'] + "\n" + \
                    solve(res.didyoumeans['didyoumean'][0]['#text'])

        return oops

    for ans in res.results:
        solution += ans.title + ': \n'
        n = len(list(ans.subpod))

        for i, sol in enumerate(list(ans.subpod)):
            solution += (str(i + 1) + ": " if n > 1 else ""
                         ) + sol.plaintext + '\n'

    return solution


def test():
    print(solve("навуходоносор"))
    print(solve("solve x^2+3x+2=0"))
    print(solve("solve my homework, please x^2+3x+2=0"))
    print(solve("integrate x^3+2+x+sin(x+x^2)"))
    print(solve("derive x^2+2"))
    print(solve("what's the meaning of life?"))


# test()