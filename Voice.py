# from gensim.models import Word2Vec

import re
# from gensim.parsing import PorterStemmer

import requests
import uuid
from bs4 import BeautifulSoup

API = "b2532bb7-0ce3-40d2-bc99-b2c9068564b3"

######################
#   Speech to text   #
######################

# this is unique idenifier of user
# API assumes that APPLICATION makes requests and we can mark them
UUID = str(uuid.uuid4()).replace("-", "")


def ask_yandex(wave_file, uid, lang="en-US"):
    # API doc
    # https://tech.yandex.ru/speechkit/cloud/doc/guide/concepts/asr-http-request-docpage/
    url = "https://asr.yandex.net/asr_xml?uuid={}&key={}&topic={}&lang={}&disableAntimat={}"
    url = url.format(uid, API, "notes", lang, "true")

    # just read raw information of the container
    data = open(wave_file, "rb").read()
    headers = {'Content-Type': 'audio/ogg;codecs=opus', 'Content-Length': str(len(data))}

    # do post request of data
    resp = requests.post(url, data=data, headers=headers)

    # parse answers
    dom = BeautifulSoup(resp.text, "lxml")
    result = dict((var.string, float(var['confidence']))
                  for var
                  in dom.html.body.recognitionresults.findAll("variant"))
    return result

def analyse_responce(responce):
    confidence = -1000
    phrase = ""
    for i in responce:
        if responce[i] > confidence:
            confidence = responce[i]
            phrase = i
    return phrase

# responce = ask_yandex("211586712.wav", str(uuid.uuid4()).replace("-", ""))

# print(responce)

# analyse_responce(ask_yandex(ask_yandex("211586712.wav", str(uuid.uuid4()).replace("-", ""))))

# print(analyse_responce(responce))


def write_to_text(responce):
    fil = open(STORAGE, "w")
    fil.write(analyse_responce(responce)+"\n")
    fil.close()

######################
#\\\\\\\\\\\\\\\\\\\\#
######################
