# -*- coding: utf-8 -*-
###############################
####_YOU_NEED_THIS_COMMENT_####
###############################
'''
*Subject (Who)
NP - Noun Phrase.
--/NN - Noun, singular or mass
--/NNS - Noun, plural
--/NNP - Proper noun, singular
--/NNPS - Proper noun, plural
--/NX - Used within certain complex NPs to mark the head of the NP. Corresponds very roughly to N-bar level but used quite

*Action (What)
VP - Vereb Phrase.
--/VB - Verb, base form
--/VBD - Verb, past tense
--/VBG - Verb, gerund or present participle
--/VBN - Verb, past participle
--/VBP - Verb, non-3rd person singular present
--/VBZ - Verb, 3rd person singular present

*Object (With that)
--/PRP - Personal pronoun
--/PRP$ - Possessive pronoun (prolog version PRP-S)

*Property (Adj)
ADJP - Adjective Phrase.
--/JJ - Adjective
--/JJR - Adjective, comparative
--/JJS - Adjective, superlative
'''
###############################
############_END_##############
###############################
noun_types = ["NN", "NNP", "NNPS", "NNS", "PRP", "PRP$"]
verb_types = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]
adjective_types = ["JJ", "JJR", "JJS"]

import nltk
from stat_parser import Parser
import re



def preprocess(line):
    line = re.sub(r"\[([A-Za-z0-9_\-]+)\]", "SOMEONE", line)
    line = re.sub(r"\"([A-Za-z0-9_\-]+)\"", "SOMETHING", line)

    line = re.sub(r"^([A-Za-z0-9_\-]+)\s*,", ", ", line)
    pre = line
    line = re.sub(r",\s*([A-Za-z0-9_\-]+)\s*," , ", ", line)
    while pre != line:
        pre = line
        line = re.sub(r",\s*([A-Za-z0-9_\-]+)\s*,", ", ", line)

    line = re.sub(r"([A-Za-z0-9_\-]+)'([A-Za-z0-9_\-]+)\s", "", line)

    line = re.sub(r"([A-Za-z0-9_\-]+)/", "", line)
    line = re.sub(r"([A-Za-z0-9_\-]+)\|", "", line)
    line = line.replace("\"", "")
    line = line.replace("\'", "")
    line = line.replace("[", "")
    line = line.replace("]", "")
    line = line.replace(",", "")
    line = line.replace("\n", "")
    return line

def phrase_level_to_one_string(tree, words=[]):
    length = tree.__len__()
    if length == 1:
        return tree[0]
    for subtree in tree:
        if isinstance(subtree[0], nltk.tree.Tree):
            phrase_level_to_one_string(subtree, words)
        else:
            words.append(subtree[0])
    final_string = ""
    for w in words:
        final_string += w + " "
    final_string = final_string[0:len(final_string)-1]
    return final_string


def traverse_tree(tree, label, suitable=[]):
    for subtree in tree:
        if isinstance(subtree, nltk.tree.Tree):
            if label == subtree._label:
                if isinstance(subtree[0], str):
                    suitable.append(subtree[0])
                elif isinstance(subtree[0], nltk.tree.Tree):
                    ###

                    suitable.append(phrase_level_to_one_string(subtree, []))
                    ###
            traverse_tree(subtree, label, suitable)
        elif isinstance(subtree, str):
            # print("_"+subtree+"_")
            pass
    return suitable


def fast_parse(line):
    parser = Parser()
    line = preprocess(line)
    trees = parser.parse(line)

    sentence_label = trees._label

    Noun = []
    for x in noun_types:
        Noun.append(traverse_tree(trees, x, []))

    prp = traverse_tree(trees, "PRP", [])
    if prp == []:
        prp = [Noun.pop(0)]
    Subject = prp
    Action = []
    for x in verb_types:
        Action.append(traverse_tree(trees, x, []))
    Object = Noun
    Property = []
    for x in adjective_types:
        Property.append(traverse_tree(trees, x, []))

    Subject, Object = Object, Subject

    ##DELETE ALL EMPTY INSTANCES
    Object = [y for x in Object for y in x]
    Subject = [y for x in Subject for y in x]
    Action = [y for x in Action for y in x]
    Property = [y for x in Property for y in x]

    # print("Subject:" + str(Subject))
    # print("Action:" + str(Action))
    # print("Object:" + str(Object))
    # print("Property:" + str(Property))


# fast_parse("translate to Tatar")
# fast_parse("calculate one plus one")
