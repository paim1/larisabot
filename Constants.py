# -*- coding: utf-8 -*-

APPLICATION_NAME = "LARISA BOT"
APP_NAME = "Larisa"
STD_MATCHES_BOARD = 21

# STATES
MAIN_MENU = "MAIN_MENU"
GAME = "GAME"
MATH = "MATH"
TALK = "TALK"
PIC = "PIC"
CHOOSE_SIDE = "CHOOSE_SIDE"
TRANSLATION = "TRANSLATION"
CHOOSE_LANG = "CHOOSE_LANG"

# GAMES
MATCHES = "21_Matches"
TIC_TAC_TOE = "3x3"
TIC_TAC_TOE_5_IN_ROW = "Five_in_a_row"

# BUTTONS
SOLVE_MATH = "Solve math"
PLAY_GAME = "Play game"
TRANSLATE = "Translate"
PICTURES = "Process image"
ONE = "1"
TWO = "2"
THREE = "3"
BACK = u"🔙Back"

# MESSAGES
INVITATION_TO_INPUT_FORMULA = ("I can solve your math hometask for "
                               "you!\nJust give me the formula and i'll "
                               "provide you a solution!")
GO_TO_MAIN_MENU = "OK! lets go to main menu!"
INVITATION_TO_TRANSLATE = "You can translate something! Choose the language!)"
INVITATION_TO_SEND_PICTURE = "Here you can load a picture of celebrity and i can recognize a person!"
EMPTY = "   "
X = u"❌"
O = u"⚪"
MSG_TIC_TAC_TOE = "Tic tac toe 3x3"
MSG_CHOOSE_SIDE = "Ok, you can choose your side in this game! :)"
MSG_USE_MENU = "Oh, you know, you always can use menu!"
MSG_YOU_CAN_GO_BACK = "you can return to main menu whenever you want"
MSG_FIVE_IN_ROW = "Five in a row"
MSG_CHOOSE_GAME = "Oh I know some games! choose one to play!"
MSG_PLAY = "Ok, lets play!"
MSG_CHOOSE_ORDER = "please select wether uour move is first or second)"
MSG_TRANSLATION = "Ok, write smth!"

# LANGS

RUSSIAN = u'Русский'
POLISH = u'Polski'
TATAR = u'Татар'
BASHKIR = u'Башкoрт'

values = {RUSSIAN: 'ru',
          POLISH: 'pl',
          TATAR: 'tt',
          BASHKIR: 'ba'}

DOCKER_ID = "86a11187cae1"
IMAGE_PATH = "/root/openface/data/celebdata/test/"
