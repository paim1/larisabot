# -*- coding: utf-8 -*-

import Constants
from telebot import types


def main_menu_keyboard():
    markup = types.ReplyKeyboardMarkup(row_width=2)
    Button_math = types.KeyboardButton(Constants.SOLVE_MATH)
    Button_play = types.KeyboardButton(Constants.PLAY_GAME)
    Button_translate = types.KeyboardButton(Constants.TRANSLATE)
    Button_picture = types.KeyboardButton(Constants.PICTURES)
    markup.add(Button_math, Button_play, Button_translate, Button_picture)
    return markup


def choose_game_keyboard():
    markup = types.ReplyKeyboardMarkup(row_width=2)
    Button_3x3 = types.KeyboardButton(Constants.TIC_TAC_TOE)
    Button_5x5 = types.KeyboardButton(Constants.TIC_TAC_TOE_5_IN_ROW)
    Button_matches = types.KeyboardButton(Constants.MATCHES)
    markup.add(Button_3x3, Button_5x5, Button_matches)
    return markup


def matches_keyboard():
    markup = types.ReplyKeyboardMarkup(row_width=3)
    Button_1 = types.KeyboardButton(Constants.ONE)
    Button_2 = types.KeyboardButton(Constants.TWO)
    Button_3 = types.KeyboardButton(Constants.THREE)
    Button_Back = types.KeyboardButton(Constants.BACK)
    markup.add(Button_1, Button_2, Button_3, Button_Back)
    return markup


def board_item_to_symbol(symbol="."):
    if symbol == "x":
        return Constants.X
    elif symbol == "o":
        return Constants.O
    else:
        return Constants.EMPTY


def inline_key_3x3(board=["." for x in range(9)]):
    key = types.InlineKeyboardMarkup(row_width=3)
    for i in range(0, 9, 3):
        key.add(types.InlineKeyboardButton(board_item_to_symbol(board[i]),
                                           callback_data="T" + str(i)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 1]),
                                           callback_data="T" + str(i + 1)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 2]),
                                           callback_data="T" + str(i + 2)))
    # Button_Back = types.KeyboardButton(Constants.BACK)
    # key.add(Button_Back)
    return key


def inline_key_5x5(board=["." for x in range(25)]):
    key = types.ReplyKeyboardMarkup(row_width=3)
    key = types.InlineKeyboardMarkup(row_width=5)
    for i in range(0, 25, 5):
        key.add(types.InlineKeyboardButton(board_item_to_symbol(board[i]),
                                           callback_data=str(i)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 1]),
                                           callback_data=str(i + 1)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 2]),
                                           callback_data=str(i + 2)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 3]),
                                           callback_data=str(i + 3)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 4]),
                                           callback_data=str(i + 4)))

    return key


def inline_key_8x8(board=["." for x in range(64)]):
    key = types.InlineKeyboardMarkup(row_width=10)
    for i in range(0, 64, 8):
        key.add(types.InlineKeyboardButton(board_item_to_symbol(board[i]),
                                           callback_data=str(i)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 1]),
                                           callback_data=str(i + 1)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 2]),
                                           callback_data=str(i + 2)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 3]),
                                           callback_data=str(i + 3)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 4]),
                                           callback_data=str(i + 4)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 5]),
                                           callback_data=str(i + 5)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 6]),
                                           callback_data=str(i + 6)),
                types.InlineKeyboardButton(board_item_to_symbol(board[i + 7]),
                                           callback_data=str(i + 7)))
    return key


def choose_game_side_keyboard():
    key = types.ReplyKeyboardMarkup(row_width=2)
    Button_X = types.KeyboardButton(Constants.X)
    Button_O = types.KeyboardButton(Constants.O)
    Button_Back = types.KeyboardButton(Constants.BACK)
    key.add(Button_X, Button_O, Button_Back)
    return key


def choose_turn_order_keyboard():
    key = types.ReplyKeyboardMarkup(row_width=2)
    Button_one = types.KeyboardButton(Constants.ONE)
    Button_two = types.KeyboardButton(Constants.TWO)
    Button_Back = types.KeyboardButton(Constants.BACK)
    key.add(Button_one, Button_two, Button_Back)
    return key


def go_back_keyboard():
    key = types.ReplyKeyboardMarkup()
    Button_Back = types.KeyboardButton(Constants.BACK)
    key.add(Button_Back)
    return key


def get_translator_lang_keyboard():
    key = types.ReplyKeyboardMarkup(row_width=2)
    Button_one = types.KeyboardButton(Constants.RUSSIAN)
    Button_two = types.KeyboardButton(Constants.TATAR)
    Button_three = types.KeyboardButton(Constants.BASHKIR)
    Button_four = types.KeyboardButton(Constants.POLISH)
    Button_Back = types.KeyboardButton(Constants.BACK)
    key.add(Button_one, Button_two, Button_three, Button_four, Button_Back)
    return key