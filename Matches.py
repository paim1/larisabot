# -*- coding: utf-8 -*-
def move(board, user=1):
    # estimate = board % 3
    for x in [1,2,3]:
        if (board-x) % 4 == 1:
            return x
    return 4-user


def estimate():
    pass


def player_move(board, matches):
    if 0 < matches <= 3:
        res = board - matches
        if res <= 0:
            res = 0
        return res
    else:
        pass


def print_board(board):
    reply = "|" * board
    reply += "   " + str(board) + " Matchsticks"
    return reply
